class Player
	include Comparable

	attr_reader :name,:score

	def initialize(player_name)
		@name = player_name
		@score = 0
	end

	def addScore(value)
		@score = @score + value
	end

	def inspect
		"PLAYER(#{name}: #{score})"
	end

	def <=>(anOther)
		@score <=> anOther.score
	end

end

#################################################################
# Represents a Tournament Management System/Database.
# Organizes Players and Matches Appropriately
################################################################
class Tournament
	attr_reader :players, :matches, :matchCount
	attr_accessor :indices

	def initialize
		@players = []
		@matches = {}
		@matchCount = 0

		@indices = {}
	end

	###############################################
	# Register Player with Tournament
	##############################################
	def addPlayer(player)
		players.push(player)
	end

	#############################################
	# Add to player_name's Score
	############################################
	def addScore(player_name, value)
		# Do not give points to NONE Players or Empty Players
		return if player_name.empty? || player_name.include?("NONE")

		players.select {|player| player.name == player_name}.each {|p| p.addScore(value)}
		players.sort! {|x,y| y<=>x}
	end

	##########################################
	# Register Match with Tournament
	#########################################
	def addMatch(id, match)
		matches[id] = [] unless matches.has_key?(id)
		matches[id].push match
	end
end

$INTERNAL_TOURNEY_LOG_FLAG = true

#######################################################################
# Take any NONE players (e.g. "NONE-1") and convert them to ""
# or some custom string if provided as the second argument.
######################################################################
def sanitize_players(list, repl_str="")
	log "Sanitizing #{list} with '#{repl_str}'"

	list.select{|s| s.include? "NONE"}.each{|s| s.replace repl_str}

	log "Sanitized to #{list}"
	return list
end

##########################################################################
# Toggle Log Flag -- Either Toggle Between True and False or explicity
# set state as argument
##########################################################################
def toggle_logging(state=nil)

	if state == nil then
		$INTERNAL_TOURNEY_LOG_FLAG = !$INTERNAL_TOURNEY_LOG_FLAG
	elsif state.is_a?(TrueClass) || state.is_a?(FalseClass)
		$INTERNAL_TOURNEY_LOG_FLAG = state
	else
		raise ArgumentError, "Bad State! Non-Boolean State Argument: #{state} => #{state.class} Class", caller
	end

	return $INTERNAL_TOURNEY_LOG_FLAG
end

#########################################################################
# Log -- Write to Console If and Only If the internal log flag is true
#########################################################################
def log(message)
	debug(message) if $INTERNAL_TOURNEY_LOG_FLAG
end

########################################################################
# Priority Log -- Ignore Log Flag Checks, Always Prints to Console
########################################################################
def plog(message)
	debug message
end

def isAlpha(text)
	return text.match(/^[[:alpha:]]$/)
end

def ymlify(file)
	return file + ".yml" unless File.extname(file) == ".yml"
	return file
end
