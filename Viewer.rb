require 'yaml'
require './Tournament'

Shoes.app(title: "Viewer - TinyTron", width: 800, height: 600, resizable: false) do
	@tourneyPath = "NONE"
	@roomID = "NONE"
	@counter = 0

	@currentIndex = 0

	#Information Panel
	flow(width: 1.0, height: 0.09) do
		fill snow
		flow(width: 0.5) do
			@titleID = title
		end
		flow(width: 0.15)
		flow(width: 0.35) do
			@timer = subtitle "Next Update In 0"
		end
	end

	#Player Rankings
	flow(width: 0.4, height: 0.91, margin_top: 20, margin_left: 20, margin_bottom: 20) do
		border black, strokewidth: 2.5
		@playerRankings = stack(scroll: true, width: 1.0, height: 1.0, margin: 10)
	end

	#Upcoming Matches
	flow(width: 0.6, height: 0.91) do
		#Current Match
		flow(height: 0.38, margin_top: 20, margin_left: 20, margin_right: 20, margin_bottom: 20) do
			border black, strokewidth: 2.5
			stack() do
			 	caption "Current Match: "
				stack(margin_left: 30) do
					@m1 = para "", emphasis: "oblique", weight: "bold", margin_bottom: 5
					@m2 = para "", emphasis: "oblique", weight: "bold", margin_bottom: 5
					@m3 = para "", emphasis: "oblique", weight: "bold", margin_bottom: 5
					@m4 = para "", emphasis: "oblique", weight: "bold", margin_bottom: 5
				end
			end
		end
		#Next Three Matches
		flow(height: 0.62, margin_left: 20, margin_right: 20, margin_bottom: 20) do
			border black, strokewidth: 2.5
			@matches = stack(width: 1.0, height: 1.0)
		end
	end

	every(1) do

		if(@tourneyPath == "NONE")
			loop do
				alert("Please select the tournament file.")
				@tourneyPath = ask_open_file
				exit() if @tourneyPath.nil?
				break if File.exist?(@tourneyPath)
			end
			plog "Tourney Path = #{@tourneyPath}"
		end

		if(@roomID == "NONE")
			loop do
				@roomID = ask("Please enter the Room ID")
				exit() if @roomID.nil?
				@roomID.upcase!
				break if isAlpha(@roomID)
			end

			@titleID.replace(" #{@roomID}")
		end

		if(@counter == 0)
			tourneyFile = File.open(@tourneyPath,"r")
			tourney = YAML.load(tourneyFile)
			tourneyFile.close
			updateInformation tourney
			@counter = 5
		else
			@counter = @counter - 1
		end

		@timer.text = "Next Update In #{@counter}"
	end

	def updateInformation(tourney)

		@currentIndex = tourney.indices[@roomID]
		match_list = tourney.matches[@roomID]
		player_list = tourney.players

		#Update Player Rankings

		@playerRankings.clear

		player_list.each do |player|
			# Do Not Include NONE Players in Leaderboards
			next if player.name.include? "NONE"

			@playerRankings.append{
				flow(height: 22, width: 1.0) do
					flow(width: 0.8){para "#{player.name}"}
					flow(width: 0.2){para "#{player.score}"}
				end
			}
		end

		# Pad End with 5 pixels of space to prevent last player from being cut off
		@playerRankings.append{flow(height:5, width:1.0)}

		#update Matches
		currentMatch = match_list[@currentIndex]
		sanitize_players(currentMatch)
		#currentMatch.select{|s| s.include? "NONE"}.each{|s| s.replace @none_str} #Convet NONE to Empty String or Otherwise
		@m1.replace(currentMatch[0])
		@m2.replace(currentMatch[1])
		@m3.replace(currentMatch[2])
		@m4.replace(currentMatch[3])

		@matches.clear

		upComing = match_list.slice(@currentIndex+1...@currentIndex+4)
		log upComing

		upComing.each do |match|
			sanitize_players(match)
			match.each do |player|
				@matches.append{
					flow(height: 21, width: 1.0){para player}
				}
			end
			@matches.append{
				flow(height: 20, width: 1.0){para strong("----------------------------")}
			}
		end
	end

end
